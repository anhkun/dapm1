import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import Defaultadmin from './layouts/admin/Defaultadmin'
import Defaultclient from './layouts/client/Defaultclient'
import Black from './layouts/Black.vue'
library.add(fab, fas, far);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('defaultadmin-layout',Defaultadmin)
Vue.component('black-layout',Black)
Vue.component('defaultclient-layout',Defaultclient)
Vue.use(Antd);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
