import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    path:'/',
    name:'Login',
    meta:{layout:'black'},
    component:()=>import('../views/admin/Login.vue')
  },
  {
    path:'/dashboad',
    name:'DashBoad',
    meta:{layout:'defaultadmin'},
    component:()=>import('../views/admin/EventsCategory.vue')
  },


]

const router = new VueRouter({
  routes
})

export default router
